import { Component, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-testimonials-component',
  standalone: true,
  imports: [],
  templateUrl: './testimonials-component.component.html',
  styleUrl: './testimonials-component.component.css'
})
export class TestimonialsComponentComponent {
  @Input() darkMode: boolean = false;
  isMobile: boolean = false;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.isMobile = window.innerWidth <= 1023;
  }

  status = false;

  changeDescription() {
    this.status = !this.status;
  }

}
