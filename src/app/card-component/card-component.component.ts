import { Component, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-card-component',
  standalone: true,
  imports: [],
  templateUrl: './card-component.component.html',
  styleUrl: './card-component.component.css'
})
export class CardComponentComponent {
  @Input() darkMode: boolean = false;
  isMobile: boolean = false;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.isMobile = window.innerWidth <= 1023;
  }

  destinos = [
    {
      id: 1,
      nombre: 'La macarena',
      precio: 130000,
      img: 'assets/d1.jpg'
    },
    {
      id: 2,
      nombre: 'Nuquí',
      precio: 197000,
      img: 'assets/d2.jpg'
    },
    {
      id: 3,
      nombre: 'Quibdó',
      precio: 184900,
      img: 'assets/d3.jpg'
    }
  ]

}
