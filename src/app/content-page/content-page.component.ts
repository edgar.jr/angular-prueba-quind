import { Component, HostListener, Input } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { BannerComponent } from '../banner/banner.component';
import { CardComponentComponent } from '../card-component/card-component.component';
import { FooterComponent } from '../footer/footer.component';
import { FormComponentComponent } from '../form-component/form-component.component';
import { TestimonialsComponentComponent } from '../testimonials-component/testimonials-component.component';

@Component({
  selector: 'app-content-page',
  standalone: true,
  imports: [BannerComponent, CardComponentComponent,FooterComponent, FormComponentComponent,TestimonialsComponentComponent],
  templateUrl: './content-page.component.html',
  styleUrl: './content-page.component.css',
  animations: [
    trigger('smoothFadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('300ms', style({ opacity: 0 })),
      ]),
    ]),
  ]
})
export class ContentPageComponent {
  @Input() darkMode: boolean = false;
  isMobile: boolean = false;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.isMobile = window.innerWidth <= 767;
  }

  destinos = [
    {
      id: 1,
      nombre: 'La macarena',
      precio: 130000,
      img: 'assets/d1.jpg'
    },
    {
      id: 2,
      nombre: 'Nuquí',
      precio: 197000,
      img: 'assets/d2.jpg'
    },
    {
      id: 3,
      nombre: 'Quibdó',
      precio: 184900,
      img: 'assets/d3.jpg'
    }
  ]

  status = false;

  changeDescription() {
    this.status = !this.status;
  }
}
