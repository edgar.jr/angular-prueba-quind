import { Component, HostListener, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
  isMenuOpen: boolean = false;
  isMobile: boolean = false;
  @Output() isDarkMode = new EventEmitter<boolean>();
  darkMode = false;
  
  dark(isDark: boolean) {
    this.isDarkMode.emit(isDark);
    this.darkMode = isDark;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.isMobile = window.innerWidth <= 1023;
  }

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

}
